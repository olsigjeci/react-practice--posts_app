import React, { Component } from "react";

import { Route, NavLink, Switch, Redirect } from "react-router-dom";
import classes from "./Blog.module.css";
import Posts from "./Posts/Posts";
// import NewPost from "./NewPost/NewPost";
// import Anime from "animejs";
import asyncComponent from "../../hoc/asyncComponent";

const AsyncNewPost = asyncComponent(() => {
  return import("./NewPost/NewPost");
});

class Blog extends Component {
  state = { auth: true };
  render() {
    return (
      <div className={classes.Blog}>
        <header>
          <nav>
            <ul>
              <li>
                <NavLink to="/posts/" activeClassName={classes.active}>
                  Posts
                </NavLink>
                <NavLink
                  to={{
                    pathname: "/new-post",
                    hash: "#submit",
                    search: "?quick-submit=true"
                  }}
                >
                  New Post
                </NavLink>
              </li>
            </ul>
          </nav>
        </header>
        <Switch>
          {this.state.auth ? (
            <Route path="/new-post" component={AsyncNewPost} />
          ) : null}
          <Route path="/posts/" component={Posts} />
          {/* <Redirect from="/" to="/posts" /> */}
          <Route render={() => <h1>Not found</h1>} />
        </Switch>
      </div>
    );
  }
}
export default Blog;
